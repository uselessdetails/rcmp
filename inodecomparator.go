package main

type InodeComparator bool

func (i InodeComparator) Applies(item) { return item.box == DirComparator }

func (i InodeComparator) Compare
