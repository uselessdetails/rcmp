package rcmp

import "testing"

func TestUnixEntryName(t *testing.T) {
	name := "foo"
	if want, got := name, (&UnixEntry{name: name}).Name(); want != got {
		t.Fatalf("want: %v, got: %v", want, got)
	}
}

func TestUnixEntryContent(t *testing.T) {
}

func TestUnixEntryParent(t *testing.T) {
}

func TestUnixEntryExists(t *testing.T) {
	exists := &UnixEntry{name: "unixentry_test.go"}
	notExists := &UnixEntry{name: "bad"}

	t.Run("exists", func(t *testing.T) {
		if want, got := true, exists.Exists(); want != got {
			t.Fatalf("want: %v, got: %v", want, got)
		}
	})

	t.Run("not exists", func(t *testing.T) {
		if want, got := false, notExists.Exists(); want != got {
			t.Fatalf("want: %v, got: %v", want, got)
		}
	})
}

func TestUnixEntryIsDir(t *testing.T) {
}

func TestUnixEntryIsLink(t *testing.T) {
}
