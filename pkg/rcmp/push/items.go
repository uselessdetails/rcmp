package rcmp

// fixme: does this need to be a struct?

type Items struct {
	content map[string]*Item
}

func newItems() { return &Items{content: make(map[string]*Item)}}

func (i *Items) findOrCreate(name string, parent bool, box *Box) *Item {
	if !box {
		box = DirComparator
	}

	// FIXME: I suspect this is extraneous.  It breaks zipfiles
        // with foo/.  Remove it once it's settled.
        name = filepath.Abs(name)
        if r, ok := i.content[name]; ok {
		return r
	}

	i.content[name] = newItem(name, parent, box)
	return i.content[name]
}

func (i *Item) delete(name string) { delete(i.content, name) }

func (i *Item) reset() { i.content = {}
