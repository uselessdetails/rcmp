//  github.com/edsrzf/mmap-go

package rcmp

import "fmt"

// An Item represents a thing which can be compared.  These can be
// entries in the file system, like a file or directory, or in an
// archive, like an archive member. This is used for caching the
// results from calls like stat and for holding content.
type Item interface {
	// Name holds the name of the Item.
	Name() string

	// Size returns the length of the content in bytes.
	Size() int64

	// Content is a copy of the content of this item.
	Content() []byte

	// Parent points to the item which contains this Item.
	Parent() *Item

	// Exists returns true iff this item exists.
	Exists() bool

	// IsDir() returns true iff this Item contains other items.
	IsDir() bool

	// IsLink() returns true iff this Item is a link to another item.
	IsLink() bool
}

type ItemPair [2]Item

func (i ItemPair) String() string {
	name := []string{"nil", "nil"}

	for c := range i {
		if i[c] != nil {
			name[c] = i[c].Name()
		}
	}

	return fmt.Sprintf("%s & %s", name[0], name[1])
}
