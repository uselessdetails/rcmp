package rcmp

// in python, a Box subclasses Comparator

type Box struct {
}

// Comparator methods
func (b *Box) Applies() bool { return false }

func (b *Box) Compare(m *Member) Result {}

func (b *Box) Keys() []string { }

func (b *Box) OuterJoin(c *Box) {}

func (b *Box) InnerJoin(c *Box) {}
