package rcmp

import "testing"

func TestInodeComparatorName(t *testing.T) {
	if want, got := inodeComparatorName, InodeComparator(true).Name(); want != got {
		t.Fatalf("want: %v, got: %v", want, got)
	}
}

func TestInodeComparatorApplies(t *testing.T) {
	exists := UnixEntry{name: "inodecomparator_test.go"}
	alsoExists := UnixEntry{name: "inodecomparator.go"}
	notExists := UnixEntry{name: "bad"}

	t.Run("true", func(t *testing.T) {
		if want, got := true, InodeComparator(true).Applies(&Comparison{
			ItemPair: ItemPair{exists, alsoExists},
		}); want != got {
			t.Fatalf("want: %v, got: %v", want, got)
		}
	})

	t.Run("false", func(t *testing.T) {
		if want, got := false, InodeComparator(true).Applies(&Comparison{
			ItemPair: ItemPair{notExists, notExists},
		}); want != got {
			t.Fatalf("want: %v, got: %v", want, got)
		}
	})
}

func TestInodeComparatorCompare(t *testing.T) {
	exists := UnixEntry{name: "nosuchcomparator_test.go"}
	alsoExists := UnixEntry{name: "inodecomparator.go"}

	t.Run("Same", func(t *testing.T) {
		if want, got := Same, InodeComparator(true).Compare(&Comparison{
			ItemPair: ItemPair{exists, exists},
		}); want != got {
			t.Fatalf("want: %v, got: %v", want, got)
		}
	})

	t.Run("Undetermined", func(t *testing.T) {
		if want, got := Undetermined, InodeComparator(true).Compare(&Comparison{
			ItemPair: ItemPair{exists, alsoExists},
		}); want != got {
			t.Fatalf("want: %v, got: %v", want, got)
		}
	})
}
