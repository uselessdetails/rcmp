package rcmp

import "testing"

func TestFailComparatorName(t *testing.T) {
	if want, got := failName, FailComparator(true).Name(); want != got {
		t.Fatalf("want: %v, got: %v", want, got)
	}
}

func TestFailComparatorApplies(t *testing.T) {
	if want, got := true, FailComparator(true).Applies(&Comparison{}); want != got {
		t.Fatalf("want: %v, got: %v", want, got)
	}
}

func TestFailComparatorCompare(t *testing.T) {
	if want, got := Different, FailComparator(true).Compare(&Comparison{}); want != got {
		t.Fatalf("want: %v, got: %v", want, got)
	}
}
