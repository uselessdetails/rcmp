package rcmp

import "github.com/rs/zerolog/log"

// FailComparator is a trivial comparator which always applies and always claims Difference.
type FailComparator bool

const failName = "FailComparator"

func (f FailComparator) Name() string { return failName }

func (f FailComparator) Applies(_ *Comparison) bool { return true }

func (f FailComparator) Compare(_ *Comparison) Result {
	log.Debug().Msgf("FailComparator.Compare: Different")
	return Different
}
