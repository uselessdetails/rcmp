// comparator represents a single comparison heuristic, a strategy.
// Instances of comparator implement individual heuristics for
// comparing items when applied to a comparison.

package rcmp

// Comparator represents a single comparison heuristic.  Comparators
// are strategies.
type Comparator interface {
	// Name is the name of this Comparator.
	Name() string

	// Applies returns true iff it is applicable to the Comparison,
	Applies(c *Comparison) bool

	// Compare applies the Comparator to the Comparison returning a Result.
	Compare(c *Comparison) Result
}
