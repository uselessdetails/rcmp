// Copyright © 2013, 2020 K Richard Pixley
// All Rights Reserved.

package main

/*
:py:mod:`Rcmp` is a more flexible replacement for :py:mod:`filecmp`
from the standard `Python <http://python.org>`_ library.

The basic idea here is that depending on content, files don't always
have to be *entirely* bitwise identical in order to be equivalent or
"close enough" for many purposes like comparing the results of two
builds.  For example, some (broken) file formats embed a time stamp
indicating the time when a file was produced even though the file
system already tracks this information.  Build the same file twice and
the two copies will initially appear to be different due to the
embedded time stamp.  Only when the irrelevant embedded time stamp
differences are ignored do the two files show out to otherwise be the
same.

:py:mod:`Rcmp` includes a flexible extension structure to allow for precisely
these sorts of living and evolving comparisons.

Extended Path Names
===================

:py:mod:`Rcmp` is capable of recursively descending into a number
of different file types including:

* file system directories
* archival and aggregating types including:

  * `ar <http://en.wikipedia.org/wiki/Ar_%28Unix%29>`_
  * `cpio <http://en.wikipedia.org/wiki/Cpio>`_

  * `tar <http://en.wikipedia.org/wiki/Tar_%28file_format%29>`_

* compressed files including:

  * `zip <http://en.wikipedia.org/wiki/Zip_%28file_format%29>`_
  * `gzip <http://en.wikipedia.org/wiki/Gzip>`_

In order to describe file locations which may extend beyond the
traditional file system paths, :py:mod:`rcmp` introduces an extended
path naming scheme.  Traditional paths are described using the
traditional slash separated list of names, :file:`/etc/hosts`.  And
components which are included in other files, like a file located
*within* a `tar <http://en.wikipedia.org/wiki/Tar_%28file_format%29>`_
archive, are described using a sequence of brace encapsulated file
format separaters.  So, for instance, a file named :file:`foo` located
within a gzip compressed, (:file:`.gz`), tar archive named
:file:`tarchive.tar` would be described as
:file:`tarchive.tar.gz{{gzip}}tarchive.tar{{tar}}foo`.  And these can
be combined as with
:file:`/home/rich/tarchive.tar.gz{{gzip}}tarchive.tar{{tar}}foo`.

Script Usage
============

:py:mod:`Rcmp` is both a library and a command line script for driving
the library.

Class Architecture
==================

.. autoclass:: Item
   :members:

.. autoclass:: Items
   :members:

.. autoclass:: Same
   :members:

.. autoclass:: Different
   :members:

.. autoclass:: Comparator
   :members:

.. autoclass:: Box
   :members:

.. autoclass:: Comparison
   :members:

.. autoclass:: ComparisonList
   :members:

Comparators
===========

..fixme:: comparators should probably be zero instance strategies.

Listed in default order of application:

.. autoclass:: NoSuchFileComparator
.. autoclass:: InodeComparator
.. autoclass:: EmptyFileComparator
.. autoclass:: DirComparator
.. autoclass:: ArMemberMetadataComparator
.. autoclass:: BitwiseComparator
.. autoclass:: SymlinkComparator

.. autoclass:: BuriedPathComparator

.. autoclass:: ElfComparator
.. autoclass:: ArComparator
.. autoclass:: AMComparator
.. autoclass:: ConfigLogComparator
.. autoclass:: KernelConfComparator
.. autoclass:: ZipComparator
.. autoclass:: TarComparator
.. autoclass:: GzipComparator
.. autoclass:: Bz2Comparator
.. autoclass:: CpioMemberMetadataComparator
.. autoclass:: CpioComparator
.. autoclass:: DateBlotBitwiseComparator
.. autoclass:: FailComparator

Utilities
=========

.. autofunction:: date_blot
.. autofunction:: ignoring

Exceptions
==========

.. autoexception:: RcmpException
.. autoexception:: IndeterminateResult

Logging strategy:
=================

Rcmp uses the python standard logging facility.  The only non-obvious
bits are that definitive differences are logged at WARNING level.
Definitive Sames are logged at WARNING - 1.  And indefinite results
are logged at WARNING - 2.  This allows for linearly increasing
volumes of logging info starting with the information that is usually
more important first.

.. Note:: I keep thinking that it would be better to create an
   IgnoringComparator that simply returned Same.  It would make much
   of the code much simpler.  However, it would mean that we'd build
   entire trees in some cases and compare them all just to produce
   constants.  This way we clip the tree.

*/
