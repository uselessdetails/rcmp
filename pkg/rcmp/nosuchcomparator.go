package rcmp

import "github.com/rs/zerolog/log"

// NoSuchComparator is an implementation of interface Comparator.  Items are
// Different if their existence is different.  That is, if only one
// fails to exist.
type NoSuchComparator bool

const noSuchComparatorName = "NoSuchComparator"

func (f NoSuchComparator) Name() string { return noSuchComparatorName }

func (f NoSuchComparator) Applies(_ *Comparison) bool { return true }

func (f NoSuchComparator) Compare(c *Comparison) Result {
	for x := range c.ItemPair {
		if c.ItemPair[x] == nil {
			log.Fatal().Msgf("NoSuchComparator.Compare: c.ItemPair[%d] is nil", x)
		}
	}

	if c.ItemPair[0].Exists() != c.ItemPair[1].Exists() {
		log.Debug().Msgf("NoSuchComparator.Compare: Different")
		return Different
	}

	log.Trace().Msgf("NoSuchComparator.Compare: Undetermined")
	return Undetermined
}
