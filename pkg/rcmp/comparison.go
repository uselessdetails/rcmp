package rcmp

import "github.com/rs/zerolog/log"

// Comparison represents a pair of objects to be compared.  
type Comparison struct {
	// ItemPair represents the pair of Item to be compared.
	ItemPair ItemPair

	// Comparators is the ordered slice of Comparators to be used
	// in the comparison.
	Comparators []Comparator

	// If ExitASAP is true, the first difference will end the
	// comparison.  If it is not true, the comparison will
	// continue despite knowing that our aggregate result is that
	// we are Different.  This is useful for getting a complete
	// list of all differences.  Unimplemented.
	// ExitASAP bool

	// If ignore_ownerships is true, then any differences in
	// element ownerships are ignored.
	// IgnoreOwnerships

	// Ignores []string
}

// Compare compares our Items.  Run through our Comparators in order
// calling each in turn with our Items.
func (c *Comparison) Compare() Result {
	const fName = "Comparison.Compare"
	
	for _, comparator := range c.Comparators {
		if !comparator.Applies(c) {
			log.Trace().Msgf("%s: Comparator does not apply: %s: %s",
				fName, comparator.Name(), c.ItemPair)
			continue
		}

		log.Trace().Msgf("%s: Comparator applies: %s: %s", fName, comparator.Name(), c.ItemPair)

		retval := comparator.Compare(c)
		if retval != Undetermined {
			log.Trace().Msgf("%s: %s %s", fName, retval, c.ItemPair)
			// c.Reset() // FIXME
			return retval
		}
	}

	log.Trace().Msgf("%s: Undetermined result for %s", fName, c.ItemPair.String())
	return Undetermined
}
