package rcmp

const (
	undetermined = iota
	same
	different
)

var (
	// Undetermined indicates a non-authoritative answer.  This
	// Comparator could not make any authoritative statement about
	// Comparison.
	Undetermined = Result(undetermined)

	// Same indicates an authoritative answer that the items in a
	// Comparison are the same.  No further comparisons need be
	// attempted.
	Same = Result(same)

	// Different indicates an authoritative answer that the items
	// in Comparison are different.  No further comparisons need
	// be attempted.
	Different = Result(different)

	// ResultNames are strings for printing convenience.
	ResultNames = map[Result]string{
		Undetermined: "Undetermined",
		Same: "Same",
		Different: "Different",
	}
)

// Result is used as the answer from a Comparator.
type Result uint

// String converts a Result into a string representation of Result.
func (r Result) String() string { return ResultNames[r] }
