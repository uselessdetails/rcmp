package rcmp

import (
	"fmt"

	"github.com/rs/zerolog/log"
)

// InodeComparator is an implementation of interface Comparator.  Items are Same
// if they have identical devices and inodes.
type InodeComparator bool

const inodeComparatorName = "InodeComparator"

func (f InodeComparator) Name() string { return inodeComparatorName }

func (f InodeComparator) Applies(c *Comparison) bool {
	var ue [2]UnixEntry

	for i := range c.ItemPair {
		if c.ItemPair[i] == nil {
			log.Fatal().Msgf("InodeComparator.Applies: c.ItemPair[%d] is nil", i)
		}

		var ok bool
		ue[i], ok = c.ItemPair[i].(UnixEntry)
		if !ok {
			log.Trace().Msgf("InodeComparator.Applies: c.ItemPair[%d] is not a UnixEntry: %T", i, c.ItemPair[i])
			return false
		}

		if !ue[i].Exists() {
			log.Trace().Msgf("InodeComparator.Applies: ue[%d] does not exist", i)
			return false
		}
	}

	return true
}

func (f InodeComparator) Compare(c *Comparison) Result {
	var ue [2]UnixEntry
	
	for i := range c.ItemPair {
		var ok bool
		ue[i], ok = c.ItemPair[i].(UnixEntry)
		if !ok {
			msg := fmt.Sprintf("InodeComparator.Compare: c.ItemPair[%d] is not a UnixEntry: %T", i, c.ItemPair[i])
			log.Trace().Msgf(msg)
			panic(msg)
		}
	}

	if ue[0].DeviceInode() == ue[1].DeviceInode() {
		log.Debug().Msgf("InodeComparator.Compare: Same")
		return Same
	}

	log.Trace().Msgf("InodeComparator.Compare: Undetermined")
	return Undetermined
}
