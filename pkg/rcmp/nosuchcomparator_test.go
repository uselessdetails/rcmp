package rcmp

import "testing"

func TestNoSuchComparatorName(t *testing.T) {
	if want, got := noSuchComparatorName, NoSuchComparator(true).Name(); want != got {
		t.Fatalf("want: %v, got: %v", want, got)
	}
}

func TestNoSuchComparatorApplies(t *testing.T) {
	if want, got := true, NoSuchComparator(true).Applies(&Comparison{}); want != got {
		t.Fatalf("want: %v, got: %v", want, got)
	}
}

func TestNoSuchComparatorCompare(t *testing.T) {
	exists := &UnixEntry{name: "nosuchcomparator_test.go"}
	notExists := &UnixEntry{name: "bad"}

	t.Run("Different", func(t *testing.T) {
		if want, got := Different, NoSuchComparator(true).Compare(&Comparison{
			ItemPair: ItemPair{exists, notExists},
		}); want != got {
			t.Fatalf("want: %v, got: %v", want, got)
		}
	})

	t.Run("Undetermined - exist", func(t *testing.T) {
		if want, got := Undetermined, NoSuchComparator(true).Compare(&Comparison{
			ItemPair: ItemPair{exists, exists},
		}); want != got {
			t.Fatalf("want: %v, got: %v", want, got)
		}
	})

	t.Run("Undetermined - no exit", func(t *testing.T) {
		if want, got := Undetermined, NoSuchComparator(true).Compare(&Comparison{
			ItemPair: ItemPair{notExists, notExists},
		}); want != got {
			t.Fatalf("want: %v, got: %v", want, got)
		}
	})

	
}
