package rcmp

import "strings"

type Packer string

func (p Packer) Join(left, right string) string {
	return left + p.String() + right
}

func (p Packer) String() string { return string(p) }

func (p Packer) Split(s string) []string { return strings.Split(s, p.String()) }
