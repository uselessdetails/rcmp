package rcmp

import (
	"testing"
)

func TestComparisonCompare(t *testing.T) {
	t.Run("empty", func(t *testing.T) {
		if want, got := Undetermined, (&Comparison{}).Compare(); want != got {
			t.Fatalf("not Indeterminate")
		}
	})

	t.Run("fail", func(t *testing.T) {
		if want, got := Different, (&Comparison{
			Comparators: []Comparator{
				FailComparator(true),
			},
		}).Compare(); want != got {
			t.Fatalf("not Different")
		}
	})



}
