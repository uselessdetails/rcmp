package rcmp

import (
	"fmt"
	"os"
	"syscall"

	"github.com/rs/zerolog/log"
)

// UnixEntry is an implementation of the Item interface for unix file
// systems.
type UnixEntry struct {
	name string
	content []byte
	parent *Item

	fileInfo *os.FileInfo
}

// Name returns the name of this Item.
func (i UnixEntry) Name() string { return i.name }

// Content returns the content of this Item.
func (i UnixEntry) Content() []byte { return []byte{} }

func (i *UnixEntry) DeviceInode() string {
	f, err := i.Lstat()
	if err != nil {
		return "Lstat-failure"
	}

	stat, ok := f.Sys().(*syscall.Stat_t)
	if !ok {
		return "stat-failure"
	}

	return fmt.Sprintf("%d+%d", stat.Dev, stat.Ino)
}

// Exists returns true iff this Item exists.
func (i UnixEntry) Exists() bool {
	_, err := i.Lstat()
	retval := err == nil
	log.Trace().Msgf("UnixEntry.Exists: %s: %v", i.Name(), retval)
	return retval
}

// Parent returns the parent of this Item.
func (i UnixEntry) Parent() *Item { return i.parent }

// IsDir returns true iff this Item contains other Items.
func (i UnixEntry) IsDir() bool { return false }

// IsLink returns true iff this Item is a link to another Item.
func (i UnixEntry) IsLink() bool { return false }

func (i *UnixEntry) Lstat() (os.FileInfo, error) {
	if i.fileInfo != nil {
		return *i.fileInfo, nil
	}

	retval, err := os.Lstat(i.Name())
	if err == nil {
		i.fileInfo = &retval
		return *i.fileInfo, err
	}

	var f os.FileInfo
	return f, err
}

func (i UnixEntry) Size() int64 {
	fName := "UnixEntry.Size"

	f, err := i.Lstat()
	if err != nil {
		panic(fmt.Sprintf("%s: Lstat failed", fName))
	}

	return f.Size()
}
